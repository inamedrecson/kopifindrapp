import React, { Component } from 'react'
import { Container, Header, Tab, Tabs, ScrollableTab } from 'native-base';
import { StyleSheet, Text} from 'react-native';
import FrogKaffeeLocation from './FrogKaffeeLoc'
import FrogKaffeeProd from './FrogKaffeeProd'

export default class FrogKTab extends Component {
  render() {
    return (
      <Container>
        <Tabs tabBarUnderlineStyle={{ backgroundColor: "#000000" }} renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#000000" }} /> }>
          <Tab heading="Place" tabStyle={{ backgroundColor: "#000000" }} activeTabStyle={{ backgroundColor: "#000000" }} >
              < FrogKaffeeLocation />           
          </Tab>
          <Tab heading="Best Sellers" tabBarUnderlineStyle={{ backgroundColor: "#000000" }}  tabStyle={{ backgroundColor: "#000000" }} activeTabStyle={{ backgroundColor: "#000000" }}>
              < FrogKaffeeProd />
          </Tab>
        </Tabs>
      </Container>


    );
  }
}