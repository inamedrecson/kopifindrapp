import React, { Component } from 'react';
import { Image, StyleSheet} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Font } from 'expo';


export default class SereniteaProd extends Component {

state = {
    fontLoaded: false,
  };


async componentDidMount() {

    await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      });
    this.setState({ fontLoaded: true });
  }



  render() {
    return (
      <Container style= {{ backgroundColor: '#000000'}}>
      {
       
        this.state.fontLoaded ? (


        <Content>
          <Card>
            <CardItem >
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}}>Coffee Latte</Text>
                  <Text note>Coffee made with heartshape design</Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://pbs.twimg.com/media/Dt8CH0rU8AAzKgk.jpg:large'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P190.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
           <Card>
            <CardItem>
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}} >Kape Ko</Text>
                  <Text note>Coffee with Mushroom Beans</Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://cdn.cnn.com/cnnnext/dam/assets/150929101049-black-coffee-stock-exlarge-169.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
               <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>3 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P150.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
           <Card>
            <CardItem>
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}} >Caramel Moist</Text>
                  <Text note>Moist Cake topped with Black Caramel </Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://cdn-image.myrecipes.com/sites/default/files/styles/4_3_horizontal_-_1200x900/public/derbycake.jpg?itok=ux8Ar5pi'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
               <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>54 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P350.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}} >Milk Tea</Text>
                  <Text note>Milk Tea topped with Polvoron </Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://pbs.twimg.com/media/DsVlXt5U0AA-KCY.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
               <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>54 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P350.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
          
        </Content>
        ) : null 
      }
      </Container>


    );
  }
}
