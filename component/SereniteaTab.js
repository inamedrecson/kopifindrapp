import React, { Component } from 'react'
import { Container, Header, Tab, Tabs, ScrollableTab } from 'native-base';
import { StyleSheet, Text} from 'react-native'
import SereniteaProd from './SereniteaProd.js';
import SereniteaLocation from './SereniteaLocation'


export default class SereniteaTab extends Component {
  render() {
    return (
      <Container>
        <Tabs tabBarUnderlineStyle={{ backgroundColor: "#000000" }} renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#000000" }} /> }>
          <Tab heading="Place" tabStyle={{ backgroundColor: "#000000" }} activeTabStyle={{ backgroundColor: "#000000" }} >
            <SereniteaLocation />
          </Tab>
          <Tab heading="Best Sellers" tabBarUnderlineStyle={{ backgroundColor: "#000000" }}  tabStyle={{ backgroundColor: "#000000" }} activeTabStyle={{ backgroundColor: "#000000" }}>
            < SereniteaProd />
          </Tab>
        </Tabs>
      </Container>


    );
  }
}