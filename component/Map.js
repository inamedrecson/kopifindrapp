import React from 'react';
import {
  Alert,
  Platform,
  StyleSheet,
  View,
  Text
} from 'react-native';
import MapView from 'react-native-maps'

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;

const initialRegion = {
  latitude: -37.78825,
  longitude: -122.4324,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
}
var CustomMap = require('../customstyle.json')


export default class MyMapView extends React.Component {

  map = null;

  state = {
    region: {
      latitude: -37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    ready: true,
    filteredMarkers: []
  };

  setRegion(region) {
    if(this.state.ready) {
      setTimeout(() => this.map.animateToRegion(region), 1);
    }
  }

  componentDidMount() {
    console.log('Component did mount');
    this.getCurrentPosition();
  }

  getCurrentPosition() {
    try {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
          this.setRegion(region);
        },
        (error) => {
          switch (error.code) {
            case 1:
              if (Platform.OS === "ios") {
                Alert.alert("", "To locate your location enable permission for the application in Settings - Privacy - Location");
              } else {
                Alert.alert("", "To locate your location enable permission for the application in Settings - Apps - ExampleApp - Location");
              }
              break;
            default:
              Alert.alert("", "Error detecting your location");
          }
        }
      );
    } catch(e) {
      alert(e.message || "");
    }
  };

  onMapReady = (e) => {
    if(!this.state.ready) {
      this.setState({ready: true});
    }
  };

  onRegionChange = (region) => {
    console.log('onRegionChange', region);
  };

  onRegionChangeComplete = (region) => {
    console.log('onRegionChangeComplete', region);
  };

  render() {

    const { region } = this.state;
    const { children, renderMarker, markers } = this.props;

    return (
      <MapView
          showsUserLocation
          ref={ map => { this.map = map }}
          initialRegion={initialRegion}
          onMapReady={this.onMapReady}
          showsMyLocationButton={false}
          onRegionChange={this.onRegionChange}
          onRegionChangeComplete={this.onRegionChangeComplete}
          style={{flex: 1}}
          customMapStyle = {CustomMap}
      >

        <MapView.Marker 
              coordinate={{

                  latitude: 7.081684,
                  longitude: 125.611246
              }}
              title={ 'Frog Kaffee and Roastery'}
              description={'F. Torres Street, Davao City'}
              onCalloutPress={() => this.props.navigation.navigate('FKTab')}            
        />

         <MapView.Marker 
             coordinate={{ latitude: 7.086258, longitude: 125.613638 }} 
             title={'Serenitea'} 
             description={'Lacson Street, Bo. Obrero, Davao City'}
             onCalloutPress={() => this.props.navigation.navigate('Tab')}
         />
         


      </MapView>
    );
  }
}

