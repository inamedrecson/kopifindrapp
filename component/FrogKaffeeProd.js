import React, { Component } from 'react';
import { Image, StyleSheet} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Font } from 'expo';


export default class FrogKaffeeProd extends Component {

state = {
    fontLoaded: false,
  };


async componentDidMount() {

    await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      });
    this.setState({ fontLoaded: true });
  }



  render() {
    return (
      <Container style= {{ backgroundColor: '#000000'}}>
      {
       
        this.state.fontLoaded ? (


        <Content>
          <Card>
            <CardItem >
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}}>Regular Coffee</Text>
                  <Text note> Coffee with instagrammable design</Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://farm1.staticflickr.com/693/23758961912_b4f78fd92b.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P190.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
           <Card>
            <CardItem>
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}} >Crembole</Text>
                  <Text note>Frog Kaffee Ice Cream</Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://4.bp.blogspot.com/-kmCebvFdo5E/VoL3UE4IOjI/AAAAAAAAEHw/m_QkEBfox9Q/s1600/FrogKaffee-004.JPG'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
               <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>3 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P150.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
           <Card>
            <CardItem>
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}} >Wild Mushroom Cappucino</Text>
                  <Text note>Not your typical cappucino </Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://4.bp.blogspot.com/-OJwfP8DI4AA/V-QAsQlfQcI/AAAAAAABF2E/v1kMD2T9HbEu14aBYYRjVcJqatPqt_ZYgCPcB/s1600/DSC_6914.JPG'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
               <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>54 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P350.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
                <Body>
                  <Text style= {{ color: '#000000', fontWeight: 'bold'}} >Pure Coffee Latte</Text>
                  <Text note> Coffee Mug Shot </Text>
                </Body>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://dxjamgtjhgl48.cloudfront.net/uploads/article_inline_image/attachment/5b84c4de372064678a0000a6/03_FROG__caramielrush_.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
               <Left>
                <Button dark>
                  <Icon active name="thumbs-up" />
                  <Text>54 Likes</Text>
                </Button>
              </Left>
              <Right>
               <Button dark>
                <Text>Price: P350.00 </Text>
               </Button>
              </Right>
            </CardItem>
          </Card>
          
        </Content>
        ) : null 
      }
      </Container>


    );
  }
}
