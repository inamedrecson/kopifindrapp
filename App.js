import React from 'react';
import MyMapView from './component/Map';
import { createStackNavigator, createAppContainer, createBottomTabNavigator} from 'react-navigation';
import { Location, Permissions } from 'expo';
import SereniteaTab from './component/SereniteaTab'
import FrogKTab from './component/FrogKaffeeTab'


class App extends React.Component {
  render() {
    return 
    <AppContainer />;
  }
}


const RootStack = createStackNavigator(
    
    {
      MyMap: MyMapView,
      Tab: SereniteaTab,
      FKTab: FrogKTab

    },
    {
      initialRouteName: 'MyMap'
    }

); 


const AppContainer = createAppContainer(RootStack);

export default createAppContainer(RootStack);


